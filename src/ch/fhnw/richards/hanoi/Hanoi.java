package ch.fhnw.richards.hanoi;

public class Hanoi {
	private Stange A, B, C;
	public static void main(String[] args) {
		if (args.length == 1) {
			int num = Integer.parseInt(args[0]);
			if (num >= 0) {
				new Hanoi(num);
			}
		}
	}

	public Hanoi(int numDisks) {
		A = new Stange("A");
		B = new Stange("B");
		C = new Stange("C");
		
		HanoiUp lastObject = A;
		for (int i = numDisks; i >= 1; i--) {
			Scheibe s = new Scheibe(i);
			try {
				lastObject.setUp(s);
			} catch (Exception e) {
				System.out.println("Problem setup failed!");
			}
			lastObject = s;
		}
		
		try {
			System.out.println("Solution steps for " + numDisks + " disks");
			this.solve(A, B, C);
			System.out.println("Finished!");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	// Move all disks from X to Z, with the help of Y
	public void solve(HanoiUp X, HanoiUp Y, HanoiUp Z) throws Exception {
		if (X.getUp() == null) {
			// Base case - zero disks - nothing to do
		} else {
			HanoiUp bottomDisk = X.getUp();
			solve(bottomDisk, Z, Y);
			movedisk(X, Z);
			solve(Y, X, bottomDisk);
		}
	}
	
	// Move a single disk from X to Y, print the move to system.out
	public void movedisk(HanoiUp X, HanoiUp Y) throws Exception {
		// Check for legal move
		if (X.getUp() == null || X.getUp().getUp() != null || Y.getUp() != null) {
			throw new Exception("Illegal move!");
		} else {
			Scheibe s = (Scheibe) X.getUp();
			X.setUp(null);
			Y.setUp(s);
			System.out.println("Move " + s.toString() + " onto " + Y.toString());
		}
	}
}
