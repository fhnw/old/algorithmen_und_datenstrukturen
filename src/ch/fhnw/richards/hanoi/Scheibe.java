package ch.fhnw.richards.hanoi;

public class Scheibe implements HanoiUp {
	private int size;
	private Scheibe up;
	
	public Scheibe(int size) {
		this.size = size;
		up = null;
	}
	
	public int getSize() {
		return size;
	}
	
	public Scheibe getUp() {
		return up;
	}
	
	public void setUp(Scheibe up) throws Exception {
		// Do not allow placing a disk if a disk is already here
		if (this.up == null || up == null) {
			this.up = up;
		} else {
			throw new Exception(this.toString() + " ist bereits belegt");			
		}
	}
	
	public String toString() {
		return "Scheibe " + size;
	}}
