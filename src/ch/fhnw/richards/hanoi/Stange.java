package ch.fhnw.richards.hanoi;

public class Stange implements HanoiUp {
	private String name;
	private Scheibe up;
	
	public Stange(String name) {
		this.name = name;
		up = null;
	}
	
	public String getName() {
		return name;
	}
	
	public Scheibe getUp() {
		return up;
	}
	
	public void setUp(Scheibe up) throws Exception {
		// Do not allow placing a disk if a disk is already here
		if (this.up == null || up == null) {
			this.up = up;
		} else {
			throw new Exception(this.toString() + " ist bereits belegt");			
		}
	}
	
	public String toString() {
		return "Stange " + name;
	}
}
