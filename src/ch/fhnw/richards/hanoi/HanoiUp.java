package ch.fhnw.richards.hanoi;

public interface HanoiUp {
	public HanoiUp getUp();
	public void setUp(Scheibe up) throws Exception;
}
