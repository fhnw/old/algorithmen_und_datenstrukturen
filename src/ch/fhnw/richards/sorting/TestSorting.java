package ch.fhnw.richards.sorting;

import java.util.Random;

public class TestSorting {
	private int[] sortArray;
	private Random rand = new Random();

	public static void main(String[] args) {
		TestSorting test = new TestSorting();
		test.go();
	}
	
	public TestSorting() {
	}
	
	private void go() {
		double iterationTime;
		long elapsedTime = 0;
		long repititions;
		// Test for sizes from 1000 to 10,000,000 elements
		for (int exponent = 1; exponent <= 7; exponent++) {

			// Increase the number of repetitions until the run-time is at least 10 seconds
			boolean timeEnough = false;
			repititions = 1;
			while (!timeEnough) {
				repititions *= 10;
				long startTime = System.nanoTime();
				for (int repitition = 1; repitition <= repititions; repitition ++) {
					runIteration(exponent);
				}
				long endTime = System.nanoTime();
				elapsedTime = endTime - startTime;
				if (elapsedTime > 1000000000) timeEnough = true; 
			}
			
			iterationTime = (double) elapsedTime / (double) repititions;
			iterationTime = iterationTime / 1000000000.0;
			System.out.println("Using 10^" + exponent + " elements, sorting requires " + iterationTime + " seconds (" + repititions + " Iterationen)");
		}
	}

	private void runIteration(int exponent) {
		// Prep the array
		int elements = 1;
		for (int i = 1; i <= exponent; i++) elements *= 10;
		sortArray = new int[elements];
		for (int i = 0; i < elements; i++) {
			sortArray[i] = rand.nextInt();
		}
		
		// Sort the array
		selectionsort();
		
		// Verify the sort
		for (int i = 0; i < elements-1; i++) {
			if (i > i+1) System.out.println("Sort failed!"); 
		}
	}

	private void bubblesort() {
		for (int i = 0; i < (sortArray.length -1); i++) {
			for (int j = i+1; j < sortArray.length; j++) {
				if (sortArray[j] < sortArray[i]) {
					swap(i, j);
				}
			}
		}
	}
	
	private void selectionsort() {
		for (int i = 0; i < (sortArray.length -1); i++) {
			int smallest = i;
			for (int j = i+1; j < sortArray.length; j++) {
				if (sortArray[j] < sortArray[smallest]) {
					smallest = j;
				}
			}
			swap(i, smallest);
		}
	}
	
	private void selectionsort_recursive(int startPosition) {
		if (startPosition == sortArray.length-1) {
			// Base case, nothing to do
		} else {
			// find smallest
			int smallest = startPosition;
			for (int i = startPosition+1; i < sortArray.length; i++) {
				if (sortArray[i] < sortArray[smallest]) {
					smallest = i;
				}
			}
			swap(startPosition, smallest);

			// Recursion
			selectionsort_recursive(startPosition+1);
		}
	}

	private void insertionsort() {
		for (int i = 1; i < sortArray.length; i++) {
			int x = sortArray[i];
			int j = i-1;
			while (j >=0 && sortArray[j] > x) {
				sortArray[j+1] = sortArray[j];
				j--;
			}
			sortArray[j+1] = x;
		}
	}
	
	private void mergesort(int start, int end) {
		if (start == end) {
			// Base case, do nothing
		} else {
			int middle = (start + end) / 2;
			mergesort(start, middle);
			mergesort(middle+1, end);
			merge(start, middle, end);
		}
	}
	
	private void merge(int start, int middle, int end) {
		int c1 = start;
		int c2 = middle+1;
		int[] temp = new int[end+1-start];
		int c_temp = 0;
		while (c1 <= middle || c2 <= end) {
			if (c1 <= middle) {
				if (c2 <= end) {
					if (sortArray[c1] < sortArray[c2]) {
						temp[c_temp++] = sortArray[c1++];
					} else {
						temp[c_temp++] = sortArray[c2++];
					}
				} else { // copy rest from c1
					temp[c_temp++] = sortArray[c1++];
				}
			} else { // copy rest from c2
				temp[c_temp++] = sortArray[c2++];
			}
		}
		
		// Copy result back to sortArray
		for (int i = 0; i < temp.length; i++) {
			sortArray[start+i] = temp[i];
		}
	}
	
	private void quicksort(int start, int end) {
		if (start >= end) {
			// base case - finished
		} else {
			// Last element is the partitioning value
			int partitionValue = sortArray[end];
			int newPos = start; // new position of partitionValue
			
			// Swap all smaller values to the front
			for (int i = start; i < end; i++) {
				if (sortArray[i] < partitionValue) {
					swap(i, newPos++);
				}
			}
			swap(newPos, end); // move partitionValue to new pos.
			
			// Recurse
			quicksort(start, newPos-1);
			quicksort(newPos+1, end);
		}
	}
	
	private void swap(int i, int j) {
		int temp = sortArray[i];
		sortArray[i] = sortArray[j];
		sortArray[j] = temp;
	}
	
}
