package ch.fhnw.richards.datastructures;

import ch.fhnw.richards.datastructures.SinglyLinkedList2.ListError;

public class Stack2<T> {
	private SinglyLinkedList2<T> stack;

	static class StackError extends Exception {
		public StackError(String s) {
			super(s);
		}
	}

	Stack2() {
		stack = new SinglyLinkedList2<T>();
	}

	public boolean empty() {
		return stack.isEmpty();
	}

	public void push(T e) throws StackError {
		try {
			stack.add(0, e);
		} catch (ListError e1) { // passiert nie
			throw new StackError(e1.getMessage());
		}
	}

	public T pop() throws StackError {
		if (empty()) throw new StackError("Error: Stack leer!");
		try {
			return stack.remove(0);
		} catch (ListError e1) { // passiert nie
			throw new StackError(e1.getMessage());
		}
	}

	public Object peek() throws StackError {
		if (empty()) throw new StackError("Error: Stack leer!");
		try {
			return stack.get(0);
		} catch (ListError e1) { // passiert nie
			throw new StackError(e1.getMessage());
		}
	}
}
