package ch.fhnw.richards.datastructures;

public class SinglyLinkedElement2<T> {
	private T data;
	protected SinglyLinkedElement2<T> next = null;
	
	protected SinglyLinkedElement2(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
}
