package ch.fhnw.richards.datastructures;

import java.util.Iterator;

public class SinglyLinkedList2Iterator<E> implements Iterator<E> {
	SinglyLinkedElement2<E> cursor;
	
	public SinglyLinkedList2Iterator(SinglyLinkedElement2 start) {
		cursor = start;
	}
	
	@Override
	public boolean hasNext() {
		return (cursor != null);
	}

	@Override
	public E next() {
		E data = cursor.getData();
		cursor = cursor.next;
		return data;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
	}
}
