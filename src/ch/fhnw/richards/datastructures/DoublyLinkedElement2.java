package ch.fhnw.richards.datastructures;

public class DoublyLinkedElement2<T> {
	private T data;
	protected DoublyLinkedElement2<T> prev = null;
	protected DoublyLinkedElement2<T> next = null;
	
	protected DoublyLinkedElement2(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
}
