package ch.fhnw.richards.datastructures;

public class StackArray {
	private Object[] Stack;
	private int TopIndex;
	private int MaxIndex;

	static class StackError extends Exception {
		public StackError() {
			super();
		}

		public StackError(String s) {
			super(s);
		}
	}

	StackArray(int sz) {
		Stack = new Object[sz];
		TopIndex = 0;
		MaxIndex = sz - 1;
	}

	public boolean empty() {
		return TopIndex <= 0;
	}

	public boolean full() {
		return TopIndex > MaxIndex;
	}

	public void push(Object e) throws StackError {
		if (full())
			throw new StackError("Stack Überlauf!");
		Stack[TopIndex] = e;
		TopIndex++;
	}

	public Object pop() throws StackError {
		if (empty())
			throw new StackError("Error: Stack leer!");
		TopIndex--;
		return Stack[TopIndex];
	}

	public Object peek() throws StackError {
		if (empty())
			throw new StackError("Error: Stack leer!");
		return Stack[TopIndex - 1];
	}
}
