package ch.fhnw.richards.datastructures;

public class SinglyLinkedList {
	private SinglyLinkedElement head = null;
	
	public SinglyLinkedList() {
	}
	
	public boolean isEmpty() {
		return (head == null);
	}
	
	public void add(int position, Object o) {
		SinglyLinkedElement e = new SinglyLinkedElement(o);
		if (position == 0) { // special handling for first position
			e.next = head;
			head = e;
		} else {
			SinglyLinkedElement cursor = head;
			// note lack of error handling: what if position is too large?
			for (int pos = 1; pos < position; pos++) {
				cursor = cursor.next;
			}
			// insert *after* element pointed to by cursor
			e.next = cursor.next;
			cursor.next = e;
		}
	}
	
	public Object get(int position) {
		SinglyLinkedElement cursor = head;
		// note lack of error handling: what if position is too large?
		for (int pos = 0; pos < position; pos++) {
			cursor = cursor.next;
		}
		return cursor.getData();
	}
	
	public Object remove(int position) {
		Object o;
		if (position == 0) { // special handling for first position
			o = head.getData();
			head = head.next;
		} else {
			SinglyLinkedElement cursor = head;
			// note lack of error handling: what if position is too large?
			for (int pos = 1; pos < position; pos++) {
				cursor = cursor.next;
			}
			// retain data to return
			o = cursor.next.getData();
			// delete element *after* cursor
			cursor.next = cursor.next.next;
		}		
		return o;
	}
}
