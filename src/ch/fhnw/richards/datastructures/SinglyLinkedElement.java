package ch.fhnw.richards.datastructures;

public class SinglyLinkedElement {
	private Object data;
	protected SinglyLinkedElement next = null;
	
	protected SinglyLinkedElement(Object data) {
		this.data = data;
	}
	
	public Object getData() {
		return data;
	}
}
