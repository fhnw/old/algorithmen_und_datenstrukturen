package ch.fhnw.richards.datastructures;

public class QueueArray {
	private Object[] Inhalt = null;
	private boolean voll = false;
	private int Anfang = 0;
	private int Ende = 0;
	private int MaxIndex;

	static class QueueError extends Exception {
		public QueueError() {
			super();
		}

		public QueueError(String s) {
			super(s);
		}
	}

	QueueArray(int sz) {
		Inhalt = new Object[sz];
		MaxIndex = sz - 1;
	}

	public boolean empty() {
		return (Anfang == Ende) && !voll;
	}

	public boolean full() {
		return voll;
	}

	public void add(Object e) throws QueueError {
		if (full())
			throw new QueueError("Queue Überlauf!");
		Inhalt[Ende] = e;
		Ende = next(Ende);
		voll = (Anfang == Ende);
	}

	public Object remove() throws QueueError {
		if (empty())
			throw new QueueError("Fehler: Queue leer!");
		Object e = Inhalt[Anfang];
		Anfang = next(Anfang);
		voll = false;
		return e;
	}

	private int next(int n) {
		if (n == MaxIndex)
			return 0;
		return ++n;
	}
}