package ch.fhnw.richards.datastructures;

import ch.fhnw.richards.datastructures.SinglyLinkedList2.ListError;

public class DoublyLinkedList2<T> {
	private DoublyLinkedElement2<T> head = null;
	private DoublyLinkedElement2<T> tail = null;

	static class ListError extends Exception {
		public ListError(String s) {
			super(s);
		}
	}

	public boolean isEmpty() {
		return (head == null);
	}

	public void add(int position, T o) throws ListError	 {
		DoublyLinkedElement2<T> e = new DoublyLinkedElement2<T>(o);
		if (position == 0) { // special handling for first position
			e.next = head;
			e.prev = null; // unnecessary, but for completeness
			if (head != null) head.prev = e;
			head = e;
		} else {
			DoublyLinkedElement2<T> cursor = head;
			for (int pos = 1; pos < position; pos++) {
				if (cursor == null) throw new ListError("Position zu gross");
				cursor = cursor.next;
			}
			// insert *after* element pointed to by cursor
			if (cursor == null) throw new ListError("Position zu gross");
			e.next = cursor.next;
			e.prev = cursor;
			if (cursor.next != null)
				cursor.next.prev = e;
			cursor.next = e;
		}
	}

	public T get(int position) throws ListError {
		DoublyLinkedElement2<T> cursor = head;
		for (int pos = 0; pos < position; pos++) {
			if (cursor == null) throw new ListError("Position zu gross");
			cursor = cursor.next;
		}
		if (cursor == null) throw new ListError("Position zu gross");
		return cursor.getData();
	}

	public T remove(int position) throws ListError {
		T o;
		if (position == 0) { // special handling for first position
			o = head.getData();
			head = head.next;
			head.prev = null;
		} else {
			DoublyLinkedElement2<T> cursor = head;
			for (int pos = 1; pos < position; pos++) {
				if (cursor == null) throw new ListError("Position zu gross");
				cursor = cursor.next;
			}
			if (cursor == null) throw new ListError("Position zu gross");
			// retain data to return
			o = cursor.next.getData();
			// delete element *after* cursor
			cursor.next = cursor.next.next;
			if (cursor.next != null)
				cursor.next.prev = cursor;
		}
		return o;
	}
}
