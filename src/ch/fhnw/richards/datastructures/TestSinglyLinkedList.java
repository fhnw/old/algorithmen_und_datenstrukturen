package ch.fhnw.richards.datastructures;

import java.util.Iterator;

import ch.fhnw.richards.datastructures.SinglyLinkedList2.ListError;

public class TestSinglyLinkedList {

	/**
	 * @param args
	 * @throws ListError 
	 */
	public static void main(String[] args) throws ListError{
		SinglyLinkedList2<Integer> list = new SinglyLinkedList2<Integer>();
		list.add(0, new Integer(1));
		list.add(1, new Integer(3));
		list.add(0, new Integer(0));
		list.add(3, new Integer(4));
		list.add(2, new Integer(2));
		
		for (Iterator<Integer> i = list.iterator(); i.hasNext(); ) {
			System.out.println(i.next());
		}
	}

}
