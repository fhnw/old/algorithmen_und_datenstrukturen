package ch.fhnw.richards.datastructures;

import java.util.Iterator;


// Iterable is optional; it promises to implement the
// "iterator()" method. Implementing it will allow us
// to use the syntax "for (elt : list)" for our data structure
public class SinglyLinkedList2<T> implements Iterable {
	private SinglyLinkedElement2<T> head = null;

	static class ListError extends Exception {
		public ListError(String s) {
			super(s);
		}
	}

	public boolean isEmpty() {
		return (head == null);
	}

	public void add(int position, T o) throws ListError {
		SinglyLinkedElement2<T> e = new SinglyLinkedElement2<T>(o);
		if (position == 0) { // special handling for first position
			e.next = head;
			head = e;
		} else {
			SinglyLinkedElement2<T> cursor = head;
			// note lack of error handling: what if position is too large?
			for (int pos = 1; pos < position; pos++) {
				if (cursor == null) throw new ListError("Position zu gross");
				cursor = cursor.next;
			}
			// insert *after* element pointed to by cursor
			if (cursor == null) throw new ListError("Position zu gross");
			e.next = cursor.next;
			cursor.next = e;
		}
	}

	public T get(int position) throws ListError {
		SinglyLinkedElement2<T> cursor = head;
		for (int pos = 0; pos < position; pos++) {
			if (cursor == null) throw new ListError("Position zu gross");
			cursor = cursor.next;
		}
		if (cursor == null) throw new ListError("Position zu gross");
		return cursor.getData();
	}

	public T remove(int position) throws ListError {
		T o;
		if (position == 0) { // special handling for first position
			o = head.getData();
			head = head.next;
		} else {
			SinglyLinkedElement2<T> cursor = head;
			for (int pos = 1; pos < position; pos++) {
				if (cursor == null) throw new ListError("Position zu gross");
				cursor = cursor.next;
			}
			if (cursor == null) throw new ListError("Position zu gross");
			// retain data to return
			o = cursor.next.getData();
			// delete element *after* cursor
			cursor.next = cursor.next.next;
		}
		return o;
	}
	
	public Iterator<T> iterator() {
		return new SinglyLinkedList2Iterator<T>(head);
	}
}
