package ch.fhnw.richards.avltree;

//See http://java.sun.com/docs/books/tutorial/extra/generics/morefun.html
public class Node<T extends Comparable<T>> implements Comparable<T> {
	private T data;
	protected int height = 1;
	protected int balance = 0;
	protected Node<T> leftChild = null;
	protected Node<T> rightChild = null;

	protected Node(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public int compareTo(T o) {
		return data.compareTo(o);
	}

	protected boolean add(T o) {
		int compareVal = data.compareTo(o);
		boolean success = true;

		// Current heights left and right
		int leftHeight = 0;
		if (leftChild != null) leftHeight = leftChild.height;
		int rightHeight = 0;
		if (rightChild != null) rightHeight = rightChild.height;

		if (compareVal < 0) { // o > this.data
			if (rightChild == null) {
				rightChild = new Node<T>(o);
				rightHeight = 1;
			} else { // recurse
				success = rightChild.add(o);
				if (rightHeight != rightChild.height) {
					if (Math.abs(rightChild.balance) > 1) {
						rightChild = rebalance(rightChild);
					}
					rightHeight = rightChild.height;
				}
			}
		} else if (compareVal > 0) { // o < this.data
			if (leftChild == null) {
				leftChild = new Node<T>(o);
				leftHeight = 1;
			} else { // recurse
				success = leftChild.add(o);
				if (leftHeight != leftChild.height) {
					if (Math.abs(leftChild.balance) > 1) {
						leftChild = rebalance(leftChild);
					}
					leftHeight = leftChild.height;
				}
			}
		} else { // o == this.data
			success = false;
		}
		height = Math.max(leftHeight, rightHeight) + 1;
		balance = rightHeight - leftHeight;
		return success;
	}

	protected T find(T o) {
		T result = null;
		int compareResult = o.compareTo(data);
		if (compareResult < 0) {
			if (leftChild != null) result = leftChild.find(o);
		} else if (compareResult > 0) {
			if (rightChild != null) result = rightChild.find(o);
		} else { // found it!
			result = data;
		}
		return result;
	}

	protected T delete(Node<T> parent, T o) {
		T result = null;
		Node<T> newChildForParent = null;
		int compareResult = o.compareTo(data);

		// Current heights left and right
		int leftHeight = 0;
		if (leftChild != null) leftHeight = leftChild.height;
		int rightHeight = 0;
		if (rightChild != null) rightHeight = rightChild.height;

		if (compareResult != 0) {
			if (compareResult < 0) { // look left
				if (leftChild != null) {
					result = leftChild.delete(this, o);
					if (leftChild == null) {
						leftHeight = 0;
					} else {
						if (Math.abs(leftChild.balance) > 1) {
							leftChild = rebalance(leftChild);
						}
						leftHeight = leftChild.height;
					}
				}
			} else if (compareResult > 0) { // look right
				if (rightChild != null) {
					result = rightChild.delete(this, o);
					if (rightChild == null) {
						rightHeight = 0;
					} else {
						if (Math.abs(rightChild.balance) > 1) {
							rightChild = rebalance(rightChild);
						}
						rightHeight = rightChild.height;
					}
				}
			}
			height = Math.max(leftHeight, rightHeight) + 1;
			balance = rightHeight - leftHeight;
		} else { // this node should be deleted
			if (leftChild == null & rightChild == null) {
				// Case 1: this node is a leaf
				// newChildForParent = null; (from initialization)
				result = data;
			} else if (leftChild == null | rightChild == null) {
				// Case 2: this node has only one child
				if (leftChild == null) {
					newChildForParent = rightChild;
				} else { // rightChild == null
					newChildForParent = leftChild;
				}
			} else if (rightChild.leftChild == null) {
				// Case 3: this node has two children; the rightmost has no left child
				newChildForParent = rightChild;
				newChildForParent.leftChild = leftChild;
			} else {
				// Case 4: smallest element from right subtree replaces current node
				newChildForParent = rightChild.removeLeftmost(this);
				newChildForParent.rightChild = rightChild;
				newChildForParent.leftChild = leftChild;
			}

			// Put the new value into the parent node
			if (parent.leftChild == this) {
				parent.leftChild = newChildForParent;
			} else {
				parent.rightChild = newChildForParent;
			}
			newChildForParent.updateStats();
		}
		return result;
	}

	// The initial call of this method guarantees that we have at least
	// one element to the left - see cases 3 and 4 in Node.delete()
	// Hence, we only ever need to touch parent.leftChild
	protected Node<T> removeLeftmost(Node<T> parent) {
		if (leftChild != null) {
			return leftChild.removeLeftmost(this);
		} else {
			parent.leftChild = this.rightChild;
			return this;
		}
	}

	protected String traverseInOrder() {
		String left = "";
		if (leftChild != null) left = leftChild.traverseInOrder() + " - ";

		String middle = ""; // Virtual root-node has null data
		if (data != null) middle = data.toString();

		String right = "";
		if (rightChild != null) right = " - " + rightChild.traverseInOrder();
		return left + middle + right;
	}

	protected void updateStats() {
		int leftHeight = 0;
		if (leftChild != null) leftHeight = leftChild.height;
		int rightHeight = 0;
		if (rightChild != null) rightHeight = rightChild.height;
		this.height = Math.max(leftHeight, rightHeight) + 1;
		this.balance = rightHeight - leftHeight;
	}

	// Return reference to new top node of this subtree
	protected Node<T> rebalance(Node<T> child) {
		Node<T> topNode = null;
		if (child.balance == -2) { // left-subtree too high
			if (child.leftChild.balance == -1) {
				// simple rotation right
				topNode = child.leftChild;
				child.leftChild = topNode.rightChild;
				topNode.rightChild = child;
				topNode.rightChild.updateStats();
				topNode.updateStats();
			} else { // child.leftChild.balance == 1
				// double rotation right
				topNode = child.leftChild.rightChild;
				child.leftChild.rightChild = topNode.leftChild;
				topNode.leftChild = child.leftChild;
				child.leftChild = topNode.rightChild;
				topNode.rightChild = child;
				topNode.leftChild.updateStats();
				topNode.rightChild.updateStats();
				topNode.updateStats();
			}
		} else { // child.balance == 2
			if (child.rightChild.balance == 1) {
				// simple rotation left
				// simple rotation right
				topNode = child.rightChild;
				child.rightChild = topNode.leftChild;
				topNode.leftChild = child;
				topNode.leftChild.updateStats();
				topNode.updateStats();
			} else { // child.rightChild.balance == -1
				// double rotation left
				topNode = child.rightChild.leftChild;
				child.rightChild.leftChild = topNode.rightChild;
				topNode.rightChild = child.rightChild;
				child.rightChild = topNode.leftChild;
				topNode.leftChild = child;
				topNode.leftChild.updateStats();
				topNode.rightChild.updateStats();
				topNode.updateStats();
			}
		}
		return topNode;
	}

	protected void printStructure(int depth) {
		if (rightChild != null) rightChild.printStructure(depth + 1);
		for (int i = 0; i < depth; i++) {
			System.out.print("    ");
		}
		System.out.print(data.toString() + " - ");
		System.out.println();
		if (leftChild != null) leftChild.printStructure(depth + 1);
	}
}
