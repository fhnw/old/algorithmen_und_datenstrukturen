package ch.fhnw.richards.binarytree;

//See http://java.sun.com/docs/books/tutorial/extra/generics/morefun.html
public class Tree<T extends Comparable<T>> {
	// Virtual node for root; tree is rightChild
	private Node<T> root = new Node(null);

	public boolean add(T o) {
		boolean success;
		if (root.rightChild == null) {
			// Special case for an empty tree
			root.rightChild = new Node<T>(o);
			success = true;
		} else {
			// Normal case: add node to tree
			success = root.rightChild.add(o);
		}
		return success;
	}
	
	public String traverseInOrder() {
		return root.traverseInOrder();
	}
	
	public T delete(T o) {
		return root.rightChild.delete(root, o);
	}
	
	public T find(T o) {
		return root.rightChild.find(o);
	}
}
