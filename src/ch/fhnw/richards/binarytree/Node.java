package ch.fhnw.richards.binarytree;

// See http://java.sun.com/docs/books/tutorial/extra/generics/morefun.html
public class Node<T extends Comparable<T>> implements Comparable<T> {
	private T data;
	protected Node<T> leftChild = null;
	protected Node<T> rightChild = null;

	protected Node(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public int compareTo(T o) {
		return data.compareTo(o);
	}

	protected boolean add(T o) {
		int compareVal = data.compareTo(o);
		boolean success = true;
		if (compareVal < 0) { // o > this.data
			if (rightChild == null) {
				rightChild = new Node<T>(o);
			} else { // recurse
				success = rightChild.add(o);
			}
		} else if (compareVal > 0) { // o < this.data
			if (leftChild == null) {
				leftChild = new Node<T>(o);
			} else { // recurse
				success = leftChild.add(o);
			}
		} else { // o == this.data
			success = false;
		}
		return success;
	}

	protected T find(T o) {
		T result = null;
		int compareResult = o.compareTo(data);
		if (compareResult < 0) {
			if (leftChild != null) result = leftChild.find(o);
		} else if (compareResult > 0) {
			if (rightChild != null) result = rightChild.find(o);
		} else { // found it!
			result = data;
		}
		return result;
	}

	protected T delete(Node<T> parent, T o) {
		T result = null;
		Node<T> newChildForParent = null;
		int compareResult = o.compareTo(data);
		if (compareResult < 0) { // look left
			if (leftChild != null) {
				return leftChild.delete(this, o);
			}
		} else if (compareResult > 0) { // look right
			if (rightChild != null) {
				return rightChild.delete(this, o);
			}
		} else { // this node should be deleted
			if (leftChild == null & rightChild == null) {
				// Case 1: this node is a leaf
				// newChildForParent = null; (from initialization)
				result = data;
			} else if (leftChild == null | rightChild == null) {
				// Case 2: this node has only one child
				if (leftChild == null) {
					newChildForParent = rightChild;
				} else { // rightChild == null
					newChildForParent = leftChild;
				}
			} else if (rightChild.leftChild == null) {
				// Case 3: this node has two children; the rightmost has no left child
				newChildForParent = rightChild;
				newChildForParent.leftChild = leftChild;
			} else {
				// Case 4: smallest element from right subtree replaces current node
				newChildForParent = rightChild.removeLeftmost(this);
				newChildForParent.rightChild = rightChild;
				newChildForParent.leftChild = leftChild;
			}

			// Put the new value into the parent node
			if (parent.leftChild == this) {
				parent.leftChild = newChildForParent;
			} else {
				parent.rightChild = newChildForParent;
			}
		}
		return result;
	}
	
	// The initial call of this method guarantees that we have at least
	// one element to the left - see cases 3 and 4 in Node.delete()
	// Hence, we only ever need to touch parent.leftChild
	protected Node<T> removeLeftmost(Node<T> parent) {
		if (leftChild != null) {
			return leftChild.removeLeftmost(this);
		} else {
			parent.leftChild = this.rightChild;
			return this;
		}
	}

	protected String traverseInOrder() {
		String left = "";
		if (leftChild != null) left = leftChild.traverseInOrder() + " - ";

		String middle = ""; // Virtual root-node has null data
		if (data != null) middle = data.toString();

		String right = "";
		if (rightChild != null) right = " - " + rightChild.traverseInOrder();
		return left + middle + right;
	}
}
