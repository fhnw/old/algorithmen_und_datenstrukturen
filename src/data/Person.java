package data;

public class Person implements Comparable<Person> {
	private String lastName;
	private String firstName;
	private Gender gender;
	
	public static enum Gender { MALE, FEMALE };
	
	public Person(String ln, String fn, Gender g) {
		lastName = ln;
		firstName = fn;
		gender = g;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public Gender getGender() {
		return gender;
	}

	@Override
	public int compareTo(Person o) {
		int c = lastName.compareTo(o.getLastName());
		if (c == 0) {
			c = firstName.compareTo(o.getFirstName());
		}
		return c;
	}
	
	@Override
	public String toString() {
		return lastName + ", " + firstName;
	}
}
