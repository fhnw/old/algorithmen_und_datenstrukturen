package data;

public class Student extends Person {
	// Student-specific attributes here...
	
	public Student(String ln, String fn, Gender g) {
		super(ln, fn, g);
	}
	
	// Student-specific methods here...
}
