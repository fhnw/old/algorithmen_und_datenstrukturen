package data;

public class Teacher extends Person {
	// Teacher-specific attributes here...
	
	public Teacher(String ln, String fn, Gender g) {
		super(ln, fn, g);
	}
	
	// Teacher-specific methods here...
}
